/**
 * Matteo Kalogirou
 * 
 * SOURCE FILE
 * This file will contain the function definitions for the volumetric image program
 * */

#include "volumetricImage.h"
#include <fstream>
#include <sstream>
#include <cmath>

using namespace std;

//---VolImage Class Functions

VolImage::VolImage() : width(0), height(0) { }

//Destructor, Must delete every array of memory in the program
VolImage::~VolImage()
{
    for(int numImg=0; numImg<slices->size(); numImg++){
        for(int i=0; i<height;i++)
        {
                delete [] slices->at(numImg)[i];
        }
        delete [] slices->at(numImg);
    }
    delete [] slices;
}

/**
 * Populate the object with images in the stack and set
 * all member variables for the file name specified.
 * Returns true if the operation was successful and false otherwise.
 */
bool VolImage::readImages(std::string baseName)
{
    int w, h, numImg;
    //Must define the image height and width from the .data file
    try
    {
        string filename = baseName + ".data";
        ifstream dataHeaderFile (filename);                         //CLOSE dataHeaderFile
        dataHeaderFile >> w >> h >> numImg;
        dataHeaderFile.close();                                     //CLOSED dataHeaderFile
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }

    setImageDimensions(w, h);

    //For the current image slice create a new stack of slices
    slices = new vector<unsigned char**>[numImg];

    //Begin loop for the number of images
    for(int imageNumber=0; imageNumber<numImg; imageNumber++){
        //Create a new fstream object for the current image
        string currentRawFile = baseName+to_string(imageNumber)+".raw";
        string s;

        try{
            //Create a file stream object using the file name
            ifstream rawFile (currentRawFile);                                  //OPEN rawFile
            getline(rawFile, s);
            char *cstr = new char[s.size()+1];                              //CREATE cstr
            strcpy(cstr, s.c_str());
            unsigned char *ucstr = new unsigned char[s.size()+1];           //CREATE ucstr
            for(int i=0; i<s.size(); i++)
                ucstr[i] = static_cast<unsigned char>(cstr[i]);
            delete [] cstr;                                                 //DESTROY cstr
            
            //Create a new column vector
            vector<unsigned char*> *column = new vector<unsigned char*>[h];

            int counter = 0;
            //For each of the columns, fill a row
            for(int c=0; c<h; c++){
                //for every row, extract the char
                vector<unsigned char> *row = new vector<unsigned char>[w];
                for(int r=0; r<w; r++)
                {
                    row->push_back(ucstr[counter]);
                    counter++;
                }
                //Push the pointer to the first row vector element onto the column vector
                column->push_back( &*(row->begin()) );
            }
            //Push the filled column vector onto the image stack
            slices->push_back( &*(column->begin()) );
            
            delete [] ucstr;                                                //DESTROY ucstr
            rawFile.close();                                                    //CLOSE rawFile
        }
        catch(const std::exception& e)
        {
            std::cerr << e.what() << '\n';
            return false;
        }
    }

    return true;
}

/**
 * Mutator method
 * */
void VolImage::setImageDimensions(int w, int h)
{
    width = w;
    height = h;   
}

/**
 * Create a difference map between two images according to the following equation:
 * Every pixel at co-ordinate r,c is calculated as:
 *  (unsigned char)(abs((float)volume[i][r][c] - (float)volume[j][r][c])/2)
 * */
void VolImage::diffmap(int sliceI, int sliceJ, std::string output_prefix)
{
    cout << "Creating a difference map between image slices "<<sliceI<<" and "<<sliceJ << "...\n";

    //Create an output stream using the given output file name
    ofstream oFile ("./output/"+output_prefix);                        //OPEN oFILE
    unsigned char pixI, pixJ, pixOut;
    float diff;
    //for every row and every column calculate the difference between the pixel values
    for(int i=0; i<height; i++)
    {
        for(int j=0; j<width; j++)
        {
            pixI = *((slices->at(sliceI))[i]+j);
            pixJ = *((slices->at(sliceJ))[i]+j);
            diff = ((float)pixI - (float)pixJ)/2;

            pixOut = (unsigned char)( abs(diff) );
            oFile << pixOut << " ";
        }
    }
    oFile.close();                                                      //CLOSE oFile
    cout << "Completed!" << endl;
    cout << "Difference map stored in ./output/"<<output_prefix<<".raw"<< endl;
}

/**  
 *   Extract the requested slice, create a header file writing the w,h, and slice number
 *   Write this to output.data
 * */
void VolImage::extract(int sliceId, std::string output_prefix)
{
    cout << "Extracting the image at position "<< sliceId << endl;

    //create an ofstream for the header file
    ofstream outputH("./output/"+output_prefix+".data");                            //OPEN outptuH
    cout << "Creating the output header file..." << endl;
    outputH << width << " " << height <<  " " << 1;
    outputH.close();                                                    //CLOSE outputh

    //creating the output
    cout << "Creating the output.raw file..." << endl;
    ofstream outputRaw("./output/"+output_prefix+".raw");                           //OPEN outputRaw

    unsigned char pixel;
    // For every column in the slice
    for(int i =0; i<height; i++){
        //For every row in the slice
        for(int j =0; j<width; j++){
            pixel = *(slices->at(sliceId)[i]+j);
            outputRaw << pixel;
        }
    }
    cout << "Completed!"<< endl;
    cout << "Extracted the image at position "<< sliceId <<" into ./output/+output.txt"<< endl;
    outputRaw.close();                                                  //CLOSE outputRaw
}

int VolImage::volImageSize(void)
{
    //8 bits per u_char therefore 1 byte per char
    return (slices->size())*width*height;
}

int VolImage::getNumSlices(void)
{
    return slices->size();
}

void printOutputMessage(int numImg, int numBytesReq)
{
    cout << "Number of images: " << numImg <<endl;
    cout << "NUmber of bytes required: "<< numBytesReq << endl;
}