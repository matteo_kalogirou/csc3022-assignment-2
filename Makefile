CC=g++								#Compiler
FLAGS= --std=c++11

volimage:	driver.o volumetricImage.o
	$(CC) volumetricImage.o driver.o -o volimage 

%.o: %.cpp
	$(CC) -c $< $(FLAGS)

clean:
	rm -f *.o volimage