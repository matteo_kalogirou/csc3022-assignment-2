/**
 * Matteo Kalogirou
 * 
 * DRIVER
 * The main code is executed from here.
 * */

#include "volumetricImage.h"
#include <iostream>
#include <sstream>

using namespace std;

int main(int argc, char *argv[])
{

    stringstream ss;

    //Determine what the input arguments are
    string imageBase, operation, outputFileName;
    int imageI, imageJ;
    if(argc == 5){          //Extraction
        imageBase = string(argv[1]);
        operation = string(argv[2]);
        ss << argv[3];
        ss >> imageI;
        outputFileName = string(argv[4]);
    }else if(argc == 6){    //Difference
        imageBase = string(argv[1]);
        operation = string(argv[2]);
        ss << argv[3] << " "<< argv[4];
        ss >> imageI >> imageJ;    
        outputFileName = string(argv[5]);
    }else if(argc <3){
        imageBase = string(argv[1]);
    }

    // cout << "Image base = "<<imageBase << endl;
    // cout << "Operation = "<<operation << endl;
    // cout << "imageI = " << imageI << endl;
    // if(argc > 5){
    //     cout << "imageJ = " << imageJ << endl;
    // }
    // cout << "outputFileName = " << outputFileName << endl;

    //Create a new VolImage and read in the data
    VolImage* imageStack = new VolImage();
    imageStack->readImages("./brain_mri_raws/"+imageBase);
    // imageStack->readImages("./test_output/"+imageBase);

    if(operation == "-x")           //Extraction
    {
        printOutputMessage(imageStack->getNumSlices(), imageStack->volImageSize());
        imageStack->extract(imageI, outputFileName);

    }else if(operation == "-d")     //Difference
    {
        printOutputMessage(imageStack->getNumSlices(), imageStack->volImageSize());
        imageStack->diffmap(imageI, imageJ, outputFileName);

    }else{                          //Unspecified
        printOutputMessage(imageStack->getNumSlices(), imageStack->volImageSize());
    }


    return 0;
}
