Author:          Matteo Kalogirou

A volumetric image tool has been created to load a stack of 2D images and manioulate them as a group.

To run the code, first compile the program by typing the command "make" into the terminal.
The executable is named "volimage". Type "./volimage <imageBaseName>" to run the default program.
By default, the program will output the number of images in the stack as well as the number of bytes used to store the image stack.

The raw image data is stored in 'brain_mri_raws/'.
The program will automatically load the images from this directory.

The output files are stored in "output/".

The program accepts the following commands:
./volimage <imageBaseName> [-x i outputFileBaseName] [-d i j outputFileBaseName]

Where:
<imageBaseName> = the base name of the files to examine
-x = extract image at position i
-d = create difference map between images at positions i and j
i,j = image positions
outputFileBaseName = eg "mri5Extracted". Exclude the file type in the name.