/**
 * Matteo Kalogirou
 * 
 * HEADER FILE
 * This file conatins the function declarations and includes for the program
 * */

#ifndef _volumetricImage_h
#define _volumetricImage_h

//---INCLUDES
#include<vector>
#include<string>
#include<iostream>

//---CLASSES
class VolImage
{
    private:
    
    // Width and height of the individual images
    int width, height;      
    /** An array of pointer pointers which store each of the slices.
    *   Each slice is represented by a matrix (rows and colums of intensities)
    *   Each slice then poitns to an array of pointers (rows) which points to 
    *   a final array of the column values.
    * */
    std::vector<unsigned char**> *slices;

    public:
    
    //---Constructor - Default, initialise the width and height to 0
    VolImage();
    //---Destructor - free up the dynamically allocated memory
    ~VolImage();

    //---METHODS

    void setImageDimensions(int w, int h);

    /**
     * Populate the object with images in the stack and set
     * all member variables. 
     * Defined in .cpp file
     * */
    bool readImages(std::string baseName);

    /**
     * Compute the difference map between the two images I and J. The 
     * differnce is then output.
     * Defined in .cpp
     * */
    void diffmap(int sliceI, int sliceJ, std::string output_prefix);

    /**
     * Extract the specified slice, sliceID and write this to the output
     * Defined in .cpp
     * */
    void extract(int sliceId, std::string output_prefix);

    /**
     * Outputs the number of bytes which were used to store the image data
     * types incl the pointers but ignoring the vector<> container and dimensions.
    * */
    int volImageSize(void);

    int getNumSlices(void);

};

//---FUNCTION DECLARATIONS
void printOutputMessage(int numImg, int numBytesReq);

#endif
